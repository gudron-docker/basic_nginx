#!/bin/bash
set -e

ansible-playbook /opt/ansible/basic/playbook.yml

# forward request and error logs to docker log collector
ln -sf /dev/stdout /var/log/nginx/access.log
ln -sf /dev/stderr /var/log/nginx/error.log

exec /usr/sbin/nginx -c /etc/nginx/nginx.conf